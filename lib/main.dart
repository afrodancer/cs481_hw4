import 'dart:math';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Animations (Homework 4)',
      home: Scaffold(
        appBar: AppBar(title: Text('Animations (Homework 4)'),),
        body: ListView(
          children: [
            Center(
              child: SizedBox(
                width: 350,
                height: 350,
                child: LogoApp(),
              ),
            ),
            Text('Airing October 6, 2013 at 7:00 am PST',
              textAlign: TextAlign.center,
              style: TextStyle(fontStyle: FontStyle.italic),),
            Container(
              padding: EdgeInsets.all(16),
              child: Text('Because of the large company, Yggdrasill Corporation setting up '
                  'shop in the once bustling city of Zawame (沢芽市 Zawame-shi); the '
                  'community started to become more like a jōkamachi (城下町 castle town). '
                  'To escape the feeling of being under castle authority, dance crews '
                  'were formed by young people to bring joy back to the public. Along '
                  'with this is a popular game that uses the mysterious Lockseeds that '
                  'contain mysterious monsters called Inves that are said to come from '
                  'another dimension. \n\n As the game becomes more popular, tears in reality '
                  'begin to open that bring the Inves from their home dimension to Earth. '
                  'Kouta Kazuraba, a young man who recently quit his dance crew Team Gaim '
                  '(to help his sister Akira Kazuraba), finds a special Lockseed and the '
                  'Sengoku Driver to become an Armored Rider, named Armored Rider Gaim. He '
                  'must fight the sudden appearance of the Inves, but also against other the '
                  'armored-Kamen Riders known as Armored Riders, effectively becoming a '
                  'Sengoku period-like conflict for only one victor to prove themselves that '
                  'they have the right to possess the power of the Helheim Forest, the proverbial '
                  'forbidden fruit. But while adamant to obtain the Golden Fruit to save the world, '
                  'Koutas goal involves him having a predestined confrontation with Kaito who desires '
                  'the fruits power to shatter the status quo of the world.'),
            ),
          ],
        ),
      ),
    );
  }
}

class AnimatedLogo extends AnimatedWidget {
  // Make the Tweens static because they don't change.
  static final _opacityTween = Tween<double>(begin: 1, end: 0.75);
  static final _sizeTween = Tween<double>(begin: 300, end: 325);

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: Opacity(
        opacity: _opacityTween.evaluate(animation),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: _sizeTween.evaluate(animation),
          width: _sizeTween.evaluate(animation),
          child: Image.asset('assets/Kamen_Rider_Gaim_Logo.png'),
        ),
      ),
    );
  }
}

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 2), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}